# Upcord

Upcord is a Discord Bot integrated with Up Bank payloads.

# Important Clarification

This is *not* an official Up Bank integration, and the hosted version is only intended for personal use. Sharing your Personal Access Token with 3rd parties is prohibited by the Up API [Acceptable Use Policy](https://up.com.au/api-acceptable-use-policy/). That being said, once Up implements 3rd party apps/OAuth support ([Tree of Up](https://up.com.au/tree/)), the bot will be updated to use this method of authentication and it should be suitable to use one hosted bot for multiple users.

## Screenshot

// TO-ADD

## License

Licensed under AGPL 3.0. Lots of thanks to [Evan Petousis](https://github.com/epetousis) for [UpBank Webhook Ingester](https://github.com/epetousis/up-webhook-ingester) and [Consubot](https://github.com/epetousis/consubot) from which this bot was born.

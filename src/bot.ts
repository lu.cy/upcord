import {
  Client,
  CommandInteraction,
  ButtonInteraction,
  SelectMenuInteraction,
  ModalSubmitInteraction,
  Intents,
} from 'discord.js';
import {
  SlashCommandBuilder, SlashCommandSubcommandsOnlyBuilder,
} from '@discordjs/builders';
import express from 'express';

import connectDb from './redis';
import HandleHooks from './webhook';
import SetupCommands, { SetupButtons, SetupSelects, SetupModal } from './setup';

type BotButton = {
  id: string,
  handler: (interaction: ButtonInteraction) => void | Promise<unknown>,
};

type BotSelectMenu = {
  id: string,
  handler: (interaction: SelectMenuInteraction) => void | Promise<unknown>,
};

type BotCommand = {
  data: Omit < SlashCommandBuilder,
  'addSubcommand' | 'addSubcommandGroup' > | SlashCommandSubcommandsOnlyBuilder,
  handler: (interaction: CommandInteraction) => void | Promise < unknown >,
};

type BotModal = {
  id: string,
  handler: (interaction: ModalSubmitInteraction) => void | Promise<unknown>,
};

const clientId = process.env.BOT_CLIENT_ID ?? '';
const permissions = 2147493888;
const token = process.env.BOT_AUTH_TOKEN ?? '';

const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
  ],
});

const buttonExports = [
  SetupButtons(),
].flat();

const buttonDefinitions = new Map<string, BotButton>();
buttonExports.forEach((button) => buttonDefinitions.set(button.id, button));

const selectExports = [
  SetupSelects(),
].flat();

const selectDefinitions = new Map<string, BotSelectMenu>();
selectExports.forEach((select) => selectDefinitions.set(select.id, select));

const modalExports = [
  SetupModal(),
].flat();

const modalDefinitions = new Map<string, BotModal>();
modalExports.forEach((modal) => modalDefinitions.set(modal.id, modal));

const commandExports = [
  SetupCommands(),
].flat();

const commandDefinitions = new Map < string,
BotCommand >();
commandExports.forEach((command) => commandDefinitions.set(command.data.name, command));

client.once('ready', () => {
  console.log('Ready!');
  connectDb();

  if (!client.user) return;
  client.user.setPresence({ activities: [{ name: 'money counting', type: 'COMPETING' }], status: 'idle' });
});

client.on('interactionCreate', async (interaction) => {
  if (interaction.isCommand()) {
    const commandDefinition = commandDefinitions.get(interaction.commandName);

    if (!commandDefinition) {
      await interaction.reply({ content: 'Command wasn\'t found. This can happen while the bot is in the middle of an update. Try again in a few minutes.', ephemeral: true });
      return;
    }

    try {
      await commandDefinition.handler(interaction);
      if (!interaction.replied) await interaction.reply({ content: 'Some goober added a command to me that didn\'t output a reply on completion. Possibly a bug?', ephemeral: true });
    } catch (error) {
      console.error('Error occurred during app command handling:', error);
      const messageContent = { content: 'There was an error while executing this command!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  } else if (interaction.isButton()) {
    const buttonDefinition = buttonDefinitions.get(interaction.customId);

    if (!buttonDefinition) {
      const buttonIdByDelim = interaction.customId.split('|');
      if (buttonIdByDelim[0] === 'coords') {
        const coordButtonDefinition = buttonDefinitions.get(buttonIdByDelim[0]);
        if (!coordButtonDefinition) {
          await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
          return;
        }
        try {
          await coordButtonDefinition.handler(interaction);
        } catch (error) {
          console.error('Error occurred during button handling:', error);
          const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

          if (interaction.replied || interaction.deferred) {
            await interaction.followUp(messageContent);
          } else {
            await interaction.reply(messageContent);
          }
        }
      } else {
        await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
      }
      return;
    }

    try {
      await buttonDefinition.handler(interaction);
    } catch (error) {
      console.error('Error occurred during button handling:', error);
      const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  } else if (interaction.isSelectMenu()) {
    const selectDefinition = selectDefinitions.get(interaction.customId);

    if (!selectDefinition) {
      await interaction.reply({ content: 'Button action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
      return;
    }

    try {
      await selectDefinition.handler(interaction);
    } catch (error) {
      console.error('Error occurred during button handling:', error);
      const messageContent = { content: 'There was an error while executing this button action!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  } else if (interaction.isModalSubmit()) {
    const modalDefinition = modalDefinitions.get(interaction.customId);

    if (!modalDefinition) {
      await interaction.reply({ content: 'Modal action failed to run. This can happen while the bot is in the middle of an update, or if the operator broke something. Try again in a few minutes.', ephemeral: true });
      return;
    }

    try {
      await modalDefinition.handler(interaction);
    } catch (error) {
      console.error('Error occurred during modal handling:', error);
      const messageContent = { content: 'There was an error while executing this modal action!', ephemeral: true };

      if (interaction.replied || interaction.deferred) {
        await interaction.followUp(messageContent);
      } else {
        await interaction.reply(messageContent);
      }
    }
  }
});

client.login(token);

const app = express();
const port = process.env.PORT || 4096;

app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/webhook/', (req, res) => {
  HandleHooks(client, req, res);
});

app.get('/', (_req, res) => {
  if (client.user != null) {
    client.user.setPresence({ activities: [{ name: 'money counting', type: 'COMPETING' }], status: 'idle' });
  }
  res.header('Location', `https://discord.com/api/oauth2/authorize?client_id=${clientId}&scope=bot%20applications.commands&permissions=${permissions}`);
  res.status(302);
  res.send();
});

app.listen(port, () => {
  console.log(`Bot running on port ${port}`);
});

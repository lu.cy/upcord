import {
  CommandInteraction,
  ButtonInteraction,
  SelectMenuInteraction,
  MessageActionRow,
  TextInputComponent,
  Modal,
  ModalActionRowComponent,
  ModalSubmitInteraction,
} from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import axios from 'axios';
import { redisC } from './redis';

async function setup(interaction: CommandInteraction) {
  const personalAccessToken = new TextInputComponent()
    .setCustomId('personalAccessTokenInput')
    .setLabel('Your Personal Access Token, please')
    .setStyle('SHORT')
    .setMinLength(8)
    .setRequired(true);
  const modal = new Modal()
    .setCustomId('setUP')
    .setTitle('SetUP your intergration');
  const pAT = new MessageActionRow<ModalActionRowComponent>()
    .addComponents(personalAccessToken);
  modal.addComponents(pAT);
  await interaction.showModal(modal);
}

async function button(interaction: ButtonInteraction) {
  await interaction.reply({ content: 'button', ephemeral: true });
}

async function select(interaction: SelectMenuInteraction) {
  await interaction.reply({ content: 'select', ephemeral: true });
}

async function model(interaction: ModalSubmitInteraction) {
  const personalAccessToken = interaction.fields.getTextInputValue('personalAccessTokenInput');
  const friendlyName = interaction.fields.getTextInputValue('friendlyInput');
  const data = JSON.stringify({
    data: {
      attributes: {
        url: 'https://upcord.herokuapp.com/webhook/',
      },
    },
  });
  const webHook = await axios({
    method: 'POST',
    url: 'https://api.up.com.au/api/v1/webhooks',
    headers: {
      Authorization: `Bearer ${personalAccessToken}`,
      'Content-Type': 'application/json',
    },
    data,
  });
  const webHookSecretKey = webHook.data.data.attributes.secretKey;
  const webHookID = webHook.data.data.id;
  const upData = {
    userID: interaction.user.id,
    channelID: interaction.channelId,
    upPAT: personalAccessToken,
    upWHSK: webHookSecretKey,
    upWHID: webHookID,
    friendlyName,
  } as UpData;
  await redisC.hSet('upcord:users', `${webHookID}`, `${JSON.stringify(upData)}`);
  await interaction.reply({
    content: 'Set UP!',
    ephemeral: true,
  });
}

export function SetupButtons() {
  return [
    { handler: button, id: '1' },
  ];
}

export function SetupSelects() {
  return [
    { handler: select, id: '2' },
  ];
}

export function SetupModal() {
  return [
    { handler: model, id: 'setUP' },
  ];
}

export default function SetupCommands() {
  return [
    {
      handler: setup,
      data: new SlashCommandBuilder()
        .setName('setup')
        .setDescription('SetUP your intergration in this channel'),
    },
  ];
}

import { Client } from 'discord.js';
import { Request, Response } from 'express';
import crypto from 'crypto';
import axios from 'axios';
import { redisC } from './redis';
import { Relationship, WebhookResponse } from './types/webhook';

async function handleTransaction(transaction: Relationship, webHook: UpData) {
  const upTransaction = await axios({
    method: 'GET',
    url: transaction.links.related,
    headers: {
      Authorization: `Bearer ${webHook.upPAT}`,
    },
  });
  console.log(upTransaction);
}

export default async function HandleHooks(client: Client<boolean>, req: Request, res: Response) {
  if (!req.body.data) {
    res.status(400);
    res.send('up yours mouthoiled request');
    return;
  }
  const pre = await redisC.hGet('upcord:users', req.body.data.id);
  if (!pre) {
    res.status(403);
    res.send("you've got the wrong url");
    return;
  }
  const webHookData = JSON.parse(pre) as UpData;
  const hmac = crypto.createHmac('sha256', webHookData.upWHSK)
    .update(req.body);
  const signature = hmac.digest('hex');
  const webHookSignature = req.get('X-Up-Authenticity-Signature');
  if (!webHookSignature) {
    res.status(403);
    res.send('no id?');
    return;
  }

  if (!crypto.timingSafeEqual(Buffer.from(signature), Buffer.from(webHookSignature))) {
    res.status(403);
    res.send('mclovin');
  }
  res.status(200);
  res.send('W + do care + did ask + thank you');

  const moneyChannel = client.channels.cache.get(webHookData.channelID);
  if (!moneyChannel) {
    return;
  }
  const hook: WebhookResponse = req.body;
  switch (hook.data.attributes.eventType) {
    case 'TRANSACTION_CREATED': {
      const { transaction } = hook.data.relationships;
      if (!transaction) {
        return;
      }
      handleTransaction(transaction, webHookData);
      break;
      // await moneyChannel.send({ content: 'money' });
    }
    default:
  }
}

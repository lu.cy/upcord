import { createClient } from 'redis';

const redisClient = createClient({ url: process.env.REDIS_URL });
redisClient.on('error', (err) => console.error('Redis Client Error', err));

export const redisC = redisClient;

export default async function connectDb() {
  await redisClient.connect();
  console.log('Connected to redis');
}

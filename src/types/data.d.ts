type UpData = {
  userID: string,
  channelID: string,
  upPAT: string,
  upWHSK: string,
  upWHID: string,
  friendlyName: string,
};
